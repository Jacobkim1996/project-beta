import { BrowserRouter, Routes, Route } from 'react-router-dom';
import AutoList from './Inventory/AutoList';
import CreateAuto from './Inventory/CreateAuto';
import CreateModel from './Inventory/CreateModel';
import ManufacturerList from './Inventory/ManufacturerList';
import ManufacturerForm from './Inventory/ManufacturerForm';
import ModelList from './Inventory/ModelList';
import MainPage from './MainPage';
import Nav from './Nav';
import TechnicianForm from './Service/CreateTechnician';
import AppointmentList from './Service/AppointmentList';
import CreateAppointment from './Service/CreateAppointment';
import AppointmentDetail from './Service/AppointmentDetail';
import CustomerForm from './Sales/CustomerForm';
import SalesPersonForm from './Sales/SalesPersonForm';
import SalesList from './Sales/SalesList';
import SalesRecordForm from './Sales/SalesRecordForm';
import SalesPersonHistory from './Sales/SalesPersonHistory';
import TechnicianList from './Service/TechnicianList';


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage/>} />
          <Route path="automobiles">
            <Route path="" element={<AutoList/>} />
            <Route path="new" element={<CreateAuto/>} />
          </Route>
          <Route path="models">
            <Route path="" element={<ModelList/>} />
            <Route path="new" element={<CreateModel/>} />
          </Route>
          <Route path="manufacturers">
            <Route path="" element={<ManufacturerList/>} />
            <Route path="new" element={<ManufacturerForm/>} />
          </Route>
          <Route path="technician">
            <Route path="new" element={<TechnicianForm/>} />
            <Route path="list" element={<TechnicianList/>} />
          </Route>
          <Route path="appointments">
            <Route path="" element={<AppointmentList/>} />
            <Route path="new" element={<CreateAppointment/>} />
            <Route path="history" element={<AppointmentDetail/>} />
          </Route>
          <Route path="salesperson">
            <Route path="" element={<SalesPersonForm/>} />
          </Route>
          <Route path="customer">
            <Route path="" element={<CustomerForm/>} />
          </Route>
          <Route path="salesrecord">
            <Route path="" element={<SalesRecordForm/>} />
            <Route path="list" element={<SalesList/>} />
            <Route path="history" element={<SalesPersonHistory/>} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
