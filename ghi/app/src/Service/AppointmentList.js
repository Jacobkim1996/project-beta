import React from 'react'

async function cancelApp(appointment) {
  const deleteUrl = `http://localhost:8080/api/appointments/${appointment.id}/`
  const fetchConfig = {
    method: "DELETE"
  }
  const response = await fetch(deleteUrl, fetchConfig)
  window.location.reload()
}

class AppointmentList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
          appointments: [],
          // is_finished: false,
        
        } 
        
        // this.handleFinishChange = this.handleFinishChange.bind(this)
    }

    async componentDidMount() {
        const response = await fetch('http://localhost:8080/api/appointments/')
        if (response.ok) {
          const data = await response.json()
          this.setState({ appointments: data.appointments })
        }
      }  
      // finish button in progress
    //   handleFinishChange(event) {
    //     const value = event.target.value;
    //     this.setState({is_finished: value})
    // }   

    render () {
        return (
            <>
            <h1>Appointments</h1>
            <table className="table table-striped">
              <thead>
                <tr>
                  <th>Vin</th>
                  <th>Customer Name</th>
                  <th>Date/Time</th>
                  <th>Technician</th>
                  <th>Reason</th>
                  <th>Vip?</th>
                  <th>Cancel Service</th>
                  <th>Finished?</th>
                </tr>
              </thead>
              <tbody>
                {this.state.appointments.map(appointment => {
                
                  return (
                    <tr key={appointment.id}>
                      <td>{ appointment.vin }</td>
                      <td>{ appointment.customer_name }</td>
                      <td>{ appointment.date_time }</td>
                      <td>{ appointment.technician.name }</td>
                      <td>{ appointment.reason }</td>   
                      <td>{ String(appointment.is_vip) }</td>
                      <td><button className="btn btn-danger" onClick={ () => cancelApp(appointment)}>Cancel</button></td>
                      <td><button type="button" className="btn btn-success" onClick={ () => cancelApp(appointment)}>Finished</button></td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
            </>
        )        
    }
}

export default AppointmentList
