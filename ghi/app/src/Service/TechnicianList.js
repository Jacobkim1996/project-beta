import React from 'react'

class TechnicianList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            technicians: [],
            employee_number:"",

        }
        
        this.handleEmployeeNumChange = this.handleEmployeeNumChange.bind(this);
    
    }

    async componentDidMount(){
        const url = 'http://localhost:8080/api/technicians/';
        
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({technicians: data.technicians}) 
        }
    }

    handleEmployeeNumChange(event) {
        const value = event.target.value;
        this.setState({ employee_number: value });
    }
    
    render () {
        return (
            <>
                <h1>Technician List</h1> 
                <input onChange={this.handleEmployeeNumChange} required id="employee_number" name="employee_number" 
                    value={this.state.employee_number} className="form-control mr-sm-2"
                    type="search" placeholder="Search Employee Number" aria-label="Search"/>
                <table className="table table-striped">
                <thead>
                    <tr>
                    <th>Employee Number</th>
                    <th>Technician Name</th>
                    </tr>
                </thead>
                <tbody>

                    {this.state.technicians.filter(technician => {
                        if(String(technician.employee_number) === this.state.employee_number){
                            return technician   
                        }
                    }).map(technician => {
                    
                        return (
                            <tr key={technician.employee_number}>
                            <td>{ technician.employee_number }</td>
                            <td>{ technician.name }</td>
                        </tr>
                        );
                    })}
                    
                </tbody>
                </table>
            </>
        )        
    }
}
export default TechnicianList