import React from 'react';

class CreateAppointment extends React.Component {
    constructor (props) {
        super(props)
        this.state = {
            vin: '',
            customer_name: '',
            date_time: '',
            reason: '',
            technicians: [],
        }

        this.handleVinChange = this.handleVinChange.bind(this)
        this.handleCustomerNameChange = this.handleCustomerNameChange.bind(this)
        this.handleDateTimeChange = this.handleDateTimeChange.bind(this)
        this.handleReasonChange = this.handleReasonChange.bind(this)
        this.handleTechnicianChange = this.handleTechnicianChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        
    }
    
    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.technicians
    
        const URL = 'http://localhost:8080/api/appointments/';
        const fetchConfig = {
          method: "POST",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        }; 
        const response = await fetch(URL, fetchConfig);
        if (response.ok) {
          const newAppointment = await response.json();

          const cleared = {
            vin: '',
            customer_name: '',
            date_time: '',
            reason: '',
            technician: '',
              
          };
          this.setState(cleared);
        }
    }

    handleVinChange(event) {
        const value = event.target.value;
        this.setState({vin: value})
    }
    
    handleCustomerNameChange(event) {
        const value = event.target.value;
        this.setState({customer_name: value})
    }
    handleDateTimeChange(event) {
        const value = event.target.value;
        this.setState({date_time: value})
    }

    handleReasonChange(event) {
        const value = event.target.value;
        this.setState({reason: value})
    }
    handleTechnicianChange(event) {
        const value = event.target.value;
        this.setState({technician: value})
    }
    async componentDidMount() {
        const url = 'http://localhost:8080/api/technicians/';
    
        const response = await fetch(url);
    
        if (response.ok) {
          const data = await response.json();

          this.setState({technicians: data.technicians});

        }
      }

    render () {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Add Appointment</h1>
                        <form onSubmit={this.handleSubmit} id="create-appointment-form">
                        <div className="form-floating mb-3">
                            <input onChange={this.handleVinChange} value={this.state.vin} placeholder="Vin" required type="text" name="vin" id="vin" maxLength={17} className="form-control"/>
                            <label htmlFor="vin">VIN</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={this.handleCustomerNameChange} value={this.state.customer_name} placeholder="Customer Name" required type="text" name="customer_name" id="customer_name" className="form-control"/>
                            <label htmlFor="customer_name">Customer Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={this.handleDateTimeChange} value={this.state.date_time} placeholder="date_time" required type="date" name="date_time" id="date_time" className="form-control"/>
                            <label htmlFor="date_time">Date</label>
                        </div>
                        <div className="form-floating mb-3">
                        <textarea onChange={this.handleReasonChange} value={this.state.reason} placeholder="reason" name="reason" id="reason" className="form-control" rows="3"/>
                            <label htmlFor="reason">Reason for visit</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={this.handleTechnicianChange} value={this.state.technician} required name="technician" id="technician" className="form-select">
                            <option value="">Choose a Technician</option>
                            {this.state.technicians.map(technician => {
                                return (
                                    <option key={technician.employee_number} value={technician.employee_number}>
                                    {technician.name}
                                    </option>
                                )
                            })}
                            </select>
                        </div>                     
                        <button className="btn btn-primary">Add Appointment</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default CreateAppointment;
    
    
