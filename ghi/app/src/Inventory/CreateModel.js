import React from 'react';

class CreateModel extends React.Component {
    constructor (props) {
        super(props)
        this.state = {
            name: '',
            picture_url: '',
            manufacturer: '',
            manufacturers: [],
           
            
        }
        this.handleNameChange = this.handleNameChange.bind(this)
        this.handlePictureChange = this.handlePictureChange.bind(this)
        this.handleManufacturerChange = this.handleManufacturerChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        
    }
    
    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.manufacturers
        data.manufacturer_id = data.manufacturer
        delete data.manufacturer
        

        const modelURL = 'http://localhost:8100/api/models/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const response = await fetch(modelURL, fetchConfig);
        if (response.ok) {
          const createModel = await response.json();
          console.log(createModel);

          const cleared = {
            name: '',
            picture_url: '',
            manufacturer: '',
    
          };
          this.setState(cleared);
        }
    }

    handleNameChange(event) {
        const value = event.target.value;
        this.setState({name: value})
    }
    
    handlePictureChange(event) {
        const value = event.target.value;
        this.setState({picture_url: value})
    }

    handleManufacturerChange(event) {
        const value = event.target.value;
        this.setState({manufacturer: value})
    }

    async componentDidMount() {
        
        const url = 'http://localhost:8100/api/manufacturers/';
    
        const response = await fetch(url);
        
        if (response.ok) {
          const data = await response.json();
          this.setState({manufacturers: data.manufacturers});

        }
      }

    render () {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Add Model</h1>
                        <form onSubmit={this.handleSubmit} id="create-model-form">
                        <div className="form-floating mb-3">
                            <input onChange={this.handleNameChange} value={this.state.name} placeholder="model name" required type="text" name="name" id="name" className="form-control"/>
                            <label htmlFor="name">Model Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={this.handlePictureChange} value={this.state.picture_url} placeholder="picture url" required type="text" name="picture_url" id="picture_url" className="form-control"/>
                            <label htmlFor="picture_url">Picture Url</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={this.handleManufacturerChange} value={this.state.manufacturer} required name="manufacturer" id="manufacturer" className="form-select">
                            <option value="">choose a manufacturer</option>
                            {this.state.manufacturers.map(manufacturer => {
                                return (
                                    <option key={manufacturer.id} value={manufacturer.id}>
                                    {manufacturer.name}
                                    </option>
                                )
                            })}
                            </select>
                        </div>                        
                        <button className="btn btn-primary">Add to Inventory</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default CreateModel;
    
    
