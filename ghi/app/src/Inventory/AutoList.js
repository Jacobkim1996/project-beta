import React from 'react'

class AutoList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {autos: []}
    }

    async componentDidMount() {
        const response = await fetch('http://localhost:8100/api/automobiles/')
        if (response.ok) {
          const data = await response.json()
          this.setState({ autos: data.autos })
        }
      }  

    render () {
        return (
            <>
            <h1>Automobile List</h1>
            <table className="table table-striped">
              <thead>
                <tr>
                  <th>Vin</th>
                  <th>Manufacturer</th>
                  <th>Model</th>
                  <th>Year</th>
                  <th>Color</th>
                </tr>
              </thead>
              <tbody>
                {this.state.autos.map(auto => {
                  return (
                  
                    <tr key={auto.id}>
                      <td>{ auto.vin }</td>
                      <td>{ auto.model.manufacturer.name }</td>
                      <td>{ auto.model.name }</td>
                      <td>{ auto.year }</td>    
                      <td>{ auto.color }</td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
            </>
        )        
    }
}

export default AutoList
