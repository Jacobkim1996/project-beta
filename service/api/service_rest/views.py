from django.shortcuts import render
from common.json import ModelEncoder
from django.http import JsonResponse
from .models import AutomobileVO, Technician, Appointment
from django.views.decorators.http import require_http_methods
import json

class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "employee_number",
        "name",
        ]

class AutomobileVOEncoder(ModelEncoder):
    model= AutomobileVO
    properties = [
        "vin",
        ]

class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "vin",
        "customer_name",
        "date_time",
        "reason",
        "is_finished",
        "is_vip",
        "technician",
    ]
    encoders = {
        "technician": TechnicianEncoder(),
        }



#List Appointments
@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentListEncoder,
            safe=False,
        )
    #POST        
    else: 
        content = json.loads(request.body) 
        
        try:
            id = content["technician"]
            technician = Technician.objects.get(employee_number=id)
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Not a Valid Employee Number"},
                status=400,
            )
        # print("THIS IS YOUR BRAIN ON CODE", content)
        vinNum = content["vin"]
        if AutomobileVO.objects.filter(vin=vinNum).exists():
            content["is_vip"] = True
        else:
            content["is_vip"] = False

        appointment = Appointment.objects.create(**content)
       
        return JsonResponse(
            appointment,
            encoder=AppointmentListEncoder,
            safe=False,
        )
@require_http_methods(["GET"])
def api_show_appointment(request, vin):
    if request.method == "GET":
        
        appointment = Appointment.objects.filter(vin = vin)
        return JsonResponse(
            appointment,
            encoder=AppointmentListEncoder,
            safe=False,
        )

@require_http_methods(["DELETE"])
def delete_appointment(request, pk):
    if request.method == "DELETE":
        try:
            appointment = Appointment.objects.get(id=pk)
            appointment.delete()
            return JsonResponse(
                appointment,
                encoder=AppointmentListEncoder,
                safe=False,
            )
        except appointment.DoesNotExist:
            return JsonResponse({"message": "This appointment Does not exist, failed to delete"})
   


@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":

        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
        )
    
    else:
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)

        return JsonResponse(
            technician, 
            encoder=TechnicianEncoder,
            safe=False
        )
