from django.db import models
from django.urls import reverse

# Create your models here.


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    vin = models.CharField(max_length=17, unique=True)

    def __str__(self):
        return f"vin: {self.vin}"


    


class SalesPerson(models.Model):
    name = models.CharField(max_length=100)
    employee_number = models.PositiveIntegerField(unique=True)

    def get_api_url(self):
        return reverse("api_sales_person", kwargs={"pk": self.id})

    def __str__(self):
        return f"name: {self.name}"



class Customer(models.Model):
    name = models.CharField(max_length=100)
    address = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=50)

    def get_api_url(self):
        return reverse ("api_customer", kwargs={"pk": self.id})

    def __str__(self):
        return f"name: {self.name}"


class SalesRecord(models.Model):
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="sales_records",
        on_delete=models.CASCADE)

    salesperson = models.ForeignKey(
        SalesPerson,
        related_name="sales_records",
        on_delete=models.CASCADE
    )

    customer = models.ForeignKey(
        Customer,
        related_name="sales_records",
        on_delete=models.CASCADE
    )

    sale_price = models.CharField(max_length=15)

    def get_api_url(self):
        return reverse ("api_sales_record", kwargs={"pk": self.id})   